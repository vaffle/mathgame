package buu.withiphong.mathgame

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import buu.withiphong.mathgame.databinding.FragmentDeletegameBinding

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [DeleteGameFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class DeleteGameFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private lateinit var binding: FragmentDeletegameBinding
    private var correct: Int = 0
    private var incorrect: Int = 0
    private var param1: String? = null
    private var param2: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_deletegame, container, false)
        generateQuestions()
        return binding.root
    }

    private fun generateQuestions() {
        binding.apply {
            val randomNum1 = (0..10).random()
            val randomNum2 = (0..10).random()
            num1.text = randomNum1.toString()
            num2.text = randomNum2.toString()
            val ans:Int = randomNum1 - randomNum2
            generateAnswer(ans)
        }
    }

    private fun generateAnswer(ans: Int) {
        binding.apply {
            val answers = arrayOf(
                "btnAns1", "btnAns2", "btnAns3"
            )
            val btnRandom = answers[(0..1).random()]
            if (btnRandom == "btnAns1") {
                btnAns1.text = ans.toString()
                btnAns2.text = (-10..10).random().toString()
                btnAns3.text = (-10..10).random().toString()
            } else if (btnRandom == "btnAns2") {
                btnAns1.text = (-10..10).random().toString()
                btnAns2.text = ans.toString()
                btnAns3.text = (-10..10).random().toString()
            } else {
                btnAns1.text = (-10..10).random().toString()
                btnAns2.text = (-10..10).random().toString()
                btnAns3.text = ans.toString()
            }
            checkAnswer(ans)
        }
    }

    private fun FragmentDeletegameBinding.checkAnswer(ans: Int) {
        btnAns1.setOnClickListener {
            val checkAns = btnAns1.text.toString()
            if (checkAns.toInt() == ans) {
                Toast.makeText(activity, "ถูกต้อง", Toast.LENGTH_LONG).show()
                correct++
                txtCorrect.text = "ถูก : " + "${correct}"
            } else {
                Toast.makeText(activity, "ไม่ถูกต้อง", Toast.LENGTH_LONG).show()
                incorrect++
                txtIncorrect.text = "ผิด : " + "${incorrect}"
            }
            generateQuestions()
        }

        btnAns2.setOnClickListener {
            val checkAns = btnAns2.text.toString()
            if (checkAns.toInt() == ans) {
                Toast.makeText(activity, "ถูกต้อง", Toast.LENGTH_LONG).show()
                correct++
                txtCorrect.text = "ถูก : " + "${correct}"
            } else {
                Toast.makeText(activity, "ไม่ถูกต้อง", Toast.LENGTH_LONG).show()
                incorrect++
                txtIncorrect.text = "ผิด : " + "${incorrect}"
            }
            generateQuestions()
        }

        btnAns3.setOnClickListener {
            val checkAns = btnAns3.text.toString()
            if (checkAns.toInt() == ans) {
                Toast.makeText(activity, "ถูกต้อง", Toast.LENGTH_LONG).show()
                correct++
                txtCorrect.text = "ถูก : " + "${correct}"
            } else {
                Toast.makeText(activity, "ไม่ถูกต้อง", Toast.LENGTH_LONG).show()
                incorrect++
                txtIncorrect.text = "ผิด : " + "${incorrect}"
            }
            generateQuestions()
        }
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment DeleteGameFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            DeleteGameFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }
}